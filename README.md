Children of Morta
=================

[**Children of Morta**](http://childrenofmorta.com/)
(on [Steam](http://store.steampowered.com/app/330020/),
[Kickstarter](https://www.kickstarter.com/projects/deadmage/children-of-morta),
[Twitter](https://twitter.com/childrenofmorta),
[Facebook](https://www.facebook.com/ChildrenOfMorta/)
) is a PC and console game by [Dead Mage](http://www.deadmage.com/).

CoM is unreleased yet, but a exclusive demo version is available (as of Q1 of 2016)
to some Kickstarter backers. Here is the public issue tracker (and possibly the wiki)
for it. Users can (and probably should) report their problems here.